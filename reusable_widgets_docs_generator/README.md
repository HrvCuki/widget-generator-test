# Reusable widget documentation generator for Flutter

## Problem

It often happens on a project that several developers work on separate parts of applications that
contain the same elements, i.e. Widgets. The problem arises when several of them are created for the
same element of a practically identical class. The result is not so beautiful code, loss of
consistency and transparency, and double or more time spent (depending on the number of developers
and widgets).

## Suggested solution

A script that generates a web page with a list of reusable widgets. The advantage of this solution
is that all developers will be able to visually see at any time which widgets have already been
created, what they are called, and where they are located in the code. This will automatically save
a lot of time that would have been spent on something that already exists.

## Installing

1. Directly to your project root folder add this package

2. In **pubspec.yaml** under dependencies add:

```yaml
reusable_widgets_docs_generator:
  path: reusable_widgets_docs_generator
```

## Generating page with reusable widgets

1. For fast generation while developing call:

```bash
dart reusable_widgets_docs_generator/lib/reusable_widgets_docs_generator.dart
```

2. For generation with code fixes and formatting call:

```bash
bash reusable_widgets_docs_generator/generate_reusable_widgets_docs.sh
```

## generate_reusable_widgets_docs.sh content

```bash
touch lib/main_reusable_widgets.dart #Creates main_reusable_widgets, widget for displaying every reusable widget
dart reusable_widgets_docs_generator/lib/src/reusable_widgets_docs_generator_main.dart #Call the generator script
dart fix --apply lib/main_reusable_widgets.dart  #Fix dart issues
flutter format lib/main_reusable_widgets.dart #Format code
```

## How to use

Simply add **@ReusableWidget()** annotation above the widget that you want to mark as reusable

### Example

```dart
@ReusableWidget()
class ReusableCard extends StatelessWidget {
  final String text;
  final Color color;

  const ReusableCard({
    Key? key,
    required this.text,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(text),
          ],
        ),
      ),
    );
  }
}

```

You can pass parameters to @ReusableWidget():

1. fixedHeight - generates widget wrapped with **SizedBox(height: fixedHeight)**
2. widgetReplacement - replace every Widget object from constructor with **widgetReplacement**
3. colorReplacement - replace every Color from constructor with **colorReplacement**

```dart
@ReusableWidget(
  fixedHeight: 200,
  widgetReplacement: ReusableCard(
    text: "Card text",
    color: Colors.blue,
  ),
  colorReplacement: Colors.blue,
)
```

## Auto deploy

To support auto deploy to Gitlab pages, you will need to add **.gitlab-ci.yml** to your project

```yml
build_and_test_web_app:
  stage: build
  image: cirrusci/flutter:stable
  before_script:
    - flutter pub get
  script:
    - flutter build web -t lib/main_reusable_widgets.dart
  artifacts:
    paths:
      - build/web

pages:
  stage: deploy
  script:
    - cp -r build/web public
  artifacts:
    paths:
      - public
  only:
    - main

```