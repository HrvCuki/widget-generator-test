library reusable_widgets_docs_generator;

import 'dart:io';

import 'package:reusable_widgets_docs_generator/src/models/reusable_widget_generator.dart';
import 'package:reusable_widgets_docs_generator/src/utils/extract_functions.dart';

main() => findFiles();

findFiles() async {
  Directory directory = Directory('./lib');
  File outputFile = File('./lib/main_reusable_widgets.dart');
  final entities = directory.listSync(recursive: true);
  final files = getEveryDartFile(entities);
  final filesWithReusableWidgets = await filterFilesWithReusableWidgets(files);
  final themeFile = await getFileWithReusableTheme(files);
  final reusableTheme = await extractTheme(themeFile);
  final fileSections = await getReusableWidgetsFromTheEntireApp(
    filesWithReusableWidgets,
  );
  final generator = ReusableWidgetsGenerator(
    fileSections: fileSections,
    reusableTheme: reusableTheme,
  );
  final fileExist = await outputFile.exists();
  if (!fileExist) await outputFile.create();
  outputFile.writeAsString(generator.buildOutput ?? '');
}
