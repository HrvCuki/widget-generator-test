import 'package:reusable_widgets_docs_generator/src/annotations/reusable_widget.dart';
import 'package:reusable_widgets_docs_generator/src/utils/random_value_generator.dart';

class ClassAttribute {
  final String type;
  final String name;
  final bool isNamed;
  final bool isRequired;
  final dynamic defaultValue;

  ClassAttribute({
    required this.type,
    required this.name,
    required this.isNamed,
    this.isRequired = false,
    this.defaultValue,
  });

  String? buildOutput(ReusableWidget reusableWidget) {
    if (!isNamed) {
      return '${getRandomValue(type, reusableWidget)}';
    } else {
      if (isRequired) {
        return '$name: ${getRandomValue(type, reusableWidget)}';
      } else if (defaultValue != null) {
        return '$name: $defaultValue';
      }
      return null;
    }
  }
}

extension ClassAttributeExtension on List<ClassAttribute> {
  String buildOutput(ReusableWidget reusableWidget) {
    final output = StringBuffer();
    final tab = '\t' * 8;
    forEach((e) {
      final newOutput = e.buildOutput(reusableWidget);
      if (newOutput != null) output.writeln('$tab$newOutput,');
    });
    return '$output';
  }
}
