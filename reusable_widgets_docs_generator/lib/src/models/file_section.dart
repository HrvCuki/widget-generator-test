import 'package:reusable_widgets_docs_generator/src/extensions/string_extensions.dart';
import 'package:reusable_widgets_docs_generator/src/models/reusable_widget_model.dart';

class FileWithReusableWidget {
  final String filePath;
  final List<String> imports;
  final List<ReusableWidgetModel> widgets;

  FileWithReusableWidget({
    required this.filePath,
    required this.imports,
    required this.widgets,
  });

  String get buildOutput {
    const newLine = '\n';
    final buffer = StringBuffer();
    buffer.writeln(imports.buildNewLines);
    buffer.writeAll(widgets.map((e) => e.codeLines.buildNewLines), newLine);
    return '$buffer';
  }
}

extension FileSectionExtension on List<FileWithReusableWidget> {
  String get buildImports {
    const newLine = '\n';
    final buffer = StringBuffer();
    buffer.writeAll(map((e) => e.filePath), newLine);
    buffer.writeln(newLine);
    return '$buffer';
  }

  String get buildFullCode {
    const newLine = '\n';
    final buffer = StringBuffer();
    buffer.writeAll(map((e) => e.imports.buildNewLines), newLine);
    buffer.writeln(newLine);
    forEach((e) {
      buffer.writeAll(
        e.widgets.map((e) => e.codeLines.buildNewLines),
        newLine,
      );
    });
    buffer.writeln(newLine);
    return '$buffer';
  }

  List<ReusableWidgetModel> get combineReusableWidgets {
    List<ReusableWidgetModel> reusableWidgets = [];
    forEach((e) {
      reusableWidgets.addAll(e.widgets);
    });
    return reusableWidgets;
  }
}
