import 'package:reusable_widgets_docs_generator/src/extensions/string_extensions.dart';
import 'package:reusable_widgets_docs_generator/src/models/file_section.dart';
import 'package:reusable_widgets_docs_generator/src/models/reusable_theme_model.dart';
import 'package:reusable_widgets_docs_generator/src/models/reusable_widget_model.dart';

class ReusableWidgetsGenerator {
  final List<FileWithReusableWidget> fileSections;
  final ReusableThemeModel? reusableTheme;

  ReusableWidgetsGenerator({
    required this.fileSections,
    required this.reusableTheme,
  });

  /// 1. [fileSections.buildImports] only writes imports
  /// 2. [fileSections.buildFullCode] writes every reusable widget with its code
  String? get buildOutput {
    return '$foundationImport\n${reusableTheme?.imports.buildNewLines}${fileSections.buildImports}\n $buildReusableWidgetPage';
  }

  String? get buildReusableWidgetPage {
    final combinedWidgets = fileSections.combineReusableWidgets;
    return '''
void main() => runApp(const ReusableWidgetExample());

class ReusableWidgetExample extends StatelessWidget {
  const ReusableWidgetExample({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reusable widgets documentation',
      theme: theme,
      home: Scaffold(
        appBar: AppBar(title: const Text('Reusable widgets documentation')),
        body: ListView.separated(
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.symmetric(
              horizontal: kIsWeb ? MediaQuery.of(context).size.width / 5 : 16,
              vertical: 16,
            ),
            child: output[index],
          ),
          separatorBuilder: (_, __) => const Divider(height: 20),
          itemCount: ${combinedWidgets.length},
        ),
      ),
    );
  }
}

final  output = [${combinedWidgets.buildOutput}];

final theme = ${reusableTheme?.themeData}
${reusableTheme?.restOfTheFile}
''';
  }
}
