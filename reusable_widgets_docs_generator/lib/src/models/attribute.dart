class Attribute {
  final String type;
  final String name;

  Attribute({
    required this.type,
    required this.name,
  });
}
