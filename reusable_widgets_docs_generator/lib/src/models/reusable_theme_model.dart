class ReusableThemeModel {
  final String themeData;
  final String restOfTheFile;
  final List<String> imports;

  ReusableThemeModel({
    required this.themeData,
    required this.imports,
    required this.restOfTheFile,
  });
}
