import 'package:reusable_widgets_docs_generator/src/annotations/reusable_widget.dart';
import 'package:reusable_widgets_docs_generator/src/models/class_attribute.dart';

class ReusableWidgetModel {
  final String name;
  final List<String> codeLines;
  final List<ClassAttribute> attributes;
  final ReusableWidget reusableWidget;

  ReusableWidgetModel({
    required this.name,
    required this.codeLines,
    required this.attributes,
    required this.reusableWidget,
  });
}

extension ReusableWidgetExtension on List<ReusableWidgetModel> {
  String get buildOutput {
    final output = StringBuffer();
    forEach((e) {
      String wholeWidget = '';
      final text =
          "Text('${e.name}', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),";
      final widgetOutput =
          '${e.name}(\n${e.attributes.buildOutput(e.reusableWidget)}),';
      if (e.reusableWidget.fixedHeight != null) {
        final wrappedWidget = wrapWithSizedBox(
          e.reusableWidget.fixedHeight,
          widgetOutput,
        );
        wholeWidget = 'Column(children: [$text$wrappedWidget],),';
      } else {
        wholeWidget = 'Column(children: [$text$widgetOutput],),';
      }
      output.writeln(wholeWidget);
    });
    return '$output';
  }
}

String wrapWithSizedBox(double? fixedHeight, String child) {
  return 'SizedBox(height: $fixedHeight, child: $child),';
}
