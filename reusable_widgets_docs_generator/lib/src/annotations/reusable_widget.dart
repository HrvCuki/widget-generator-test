class ReusableWidget {
  final double? fixedHeight;
  final dynamic widgetReplacement;
  final dynamic colorReplacement;
  static const annotation = '@ReusableWidget';

  static const paramNames = [
    'fixedHeight',
    'widgetReplacement',
    'colorReplacement',
  ];

  const ReusableWidget({
    this.fixedHeight,
    this.widgetReplacement,
    this.colorReplacement,
  });

  factory ReusableWidget.fromAnnotationMao(Map<String, dynamic>? map) {
    if (map == null) return const ReusableWidget();
    return ReusableWidget(
      fixedHeight: double.tryParse(map['fixedHeight'] ?? ''),
      widgetReplacement: map['widgetReplacement'],
      colorReplacement: map['colorReplacement'],
    );
  }

  @override
  String toString() {
    return 'fixedHeight: $fixedHeight, widgetReplacement: $widgetReplacement';
  }
}
