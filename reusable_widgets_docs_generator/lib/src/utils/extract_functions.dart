import 'dart:io';

import 'package:reusable_widgets_docs_generator/src/annotations/reusable_theme.dart';
import 'package:reusable_widgets_docs_generator/src/annotations/reusable_widget.dart';
import 'package:reusable_widgets_docs_generator/src/extensions/string_extensions.dart';
import 'package:reusable_widgets_docs_generator/src/models/attribute.dart';
import 'package:reusable_widgets_docs_generator/src/models/class_attribute.dart';
import 'package:reusable_widgets_docs_generator/src/models/file_section.dart';
import 'package:reusable_widgets_docs_generator/src/models/reusable_theme_model.dart';
import 'package:reusable_widgets_docs_generator/src/models/reusable_widget_model.dart';

Future<ReusableThemeModel?> extractTheme(File? file) async {
  if (file == null) return null;
  final lines = await file.readAsLines();
  final indexOfAnnotation = lines.indexOf(ReusableTheme.annotation);
  final imports = lines.sublist(0, indexOfAnnotation);
  final code = lines.sublist(indexOfAnnotation + 1);
  final codeString =
      code.map((e) => e.trim()).where((e) => !e.startsWith('//')).join();
  final themeClosure = codeString.indexOf(';');
  final themeDataStart = codeString.indexOf(themeDataText);
  final themeData = codeString.substring(themeDataStart, themeClosure + 1);
  final restOfTheFile = codeString.substring(themeClosure + 1);
  return ReusableThemeModel(
    imports: imports,
    themeData: themeData,
    restOfTheFile: restOfTheFile,
  );
}

Future<List<FileWithReusableWidget>> getReusableWidgetsFromTheEntireApp(
  List<File> files,
) async {
  List<FileWithReusableWidget> allFileSections = [];
  for (final file in files) {
    final fileSections = await extractComponentsFromFile(file);
    allFileSections.add(fileSections);
  }
  return allFileSections;
}

Future<FileWithReusableWidget> extractComponentsFromFile(File file) async {
  final lines = await convertFileToLines(file);
  final List<String> imports = [];
  final List<ReusableWidgetModel> reusableWidgets = [];
  int startIndex = 0;
  int endIndex = 0;
  for (int i = 0; i < lines.length; i++) {
    final line = lines[i];
    if (line.isImport) {
      imports.add(line);
    } else if (line.startsWithReusableWidget) {
      startIndex = i;
    } else if (line.startsWithClass) {
      endIndex = i;
      final annotation = lines.sublist(startIndex, endIndex);
      final subLines = lines.sublist(endIndex);
      reusableWidgets.add(extractReusableWidget(subLines, annotation));
    }
  }
  final fileSection = FileWithReusableWidget(
    imports: imports,
    widgets: reusableWidgets,
    filePath: buildImportPathFromFile(file),
  );
  return fileSection;
}

String buildImportPathFromFile(File file) {
  final absolutePath = file.absolute.path;
  final list = absolutePath.split('/');
  final indexOfCurrentDir = list.indexOf('.');
  final appName = list[indexOfCurrentDir - 1];
  final relativePath = absolutePath.split('./lib').last;
  final importPath = "import 'package:$appName$relativePath';";
  return importPath;
}

ReusableWidgetModel extractReusableWidget(
  List<String> lines,
  List<String> annotation,
) {
  bool added = false;
  List<String> codeLines = [];
  String name = '';
  for (final line in lines) {
    if (line.startsWithClass || line.startsWithReusableWidget) {
      if (added) break;
      name = extractClassNameFromLine(line);
      added = true;
    }
    codeLines.add(line);
  }
  final annotationParameters = extractAnnotationParameters(annotation.join());
  return ReusableWidgetModel(
    name: name,
    codeLines: codeLines,
    attributes: extractAttributes(extractAttributeLines(codeLines), name),
    reusableWidget: ReusableWidget.fromAnnotationMao(
      annotationParametersToMap(annotationParameters),
    ),
  );
}

Map<String, dynamic>? annotationParametersToMap(String? annotationParameters) {
  if (annotationParameters == null) return null;
  final indexes = ReusableWidget.paramNames
      .map((e) => annotationParameters.indexOf(e))
      .where((e) => e > -1)
      .toList()
    ..sort();
  Map<String, dynamic> map = {};
  for (int i = 0; i < indexes.length; i++) {
    if (i == indexes.length - 1) {
      final extractedParamLine = annotationParameters.substring(indexes[i]);
      final entry = mapAnnotationParams(
        extractedParamLine,
        ReusableWidget.paramNames,
      );
      if (entry != null) {
        map[entry.key] = entry.value;
      }
    } else {
      final extractedParamLine =
          annotationParameters.substring(indexes[i], indexes[i + 1]);
      final entry = mapAnnotationParams(
        extractedParamLine,
        ReusableWidget.paramNames,
      );
      if (entry != null) {
        map[entry.key] = entry.value;
      }
    }
  }
  return map;
}

MapEntry<String, dynamic>? mapAnnotationParams(
  String paramLine,
  List<String> paramNames,
) {
  for (final paramName in paramNames) {
    if (paramLine.contains(paramName)) {
      final list = paramLine.split('$paramName:');
      return MapEntry(
        paramName,
        list.last.trim().removeLastOccurrenceOf(','),
      );
    }
  }
  return null;
}

String? extractAnnotationParameters(String annotationLine) {
  final parameters = annotationLine
      .getStringBetween('(', ')', fromFirstOccurrence: false)
      .trim();
  if (parameters.isEmpty) return null;
  return parameters;
}

List<String> extractAttributeLines(List<String> lines) {
  final lastIndex = lines.indexWhere((e) => e.containsWidgetBuild);
  return lines.sublist(1, lastIndex - 1);
}

List<String> extractConstructorLines(List<String> lines, String name) {
  final firstIndex = lines.indexWhere((e) => e.contains(name));
  return lines.sublist(firstIndex);
}

List<ClassAttribute> extractAttributes(
  List<String> codeLines,
  String className,
) {
  List<Attribute> attributes = [];
  for (final line in codeLines) {
    final trimLine = line.trim();
    if (trimLine.startsWithFinal) {
      final attributeValues = trimLine.split(' ');
      // print(attributeValues);
      final attribute = Attribute(
        name: attributeValues.last.removeLastChar,
        type: attributeValues[1],
      );
      attributes.add(attribute);
    } else if (className.contains(className)) {}
  }
  final constructorLines = extractConstructorLines(codeLines, className);
  final constructor = constructorLines.map((e) => e.trim()).join('');
  final namedAttributes = getNamedClassAttributes(constructor, attributes);
  final noNamedAttributes = getNoNamedClassAttributes(constructor, attributes);
  return [...noNamedAttributes, ...namedAttributes];
}

List<ClassAttribute> getNoNamedClassAttributes(
  String constructor,
  List<Attribute> attributes,
) {
  final constructorWithoutNamedArguments = constructor.removeNamedArguments;
  List<ClassAttribute> classAttributes = [];
  final noNamedAttributes =
      constructorWithoutNamedArguments.getAttributesFromConstructor('(', ')');
  for (final noNamedAttribute in noNamedAttributes) {
    for (final attribute in attributes) {
      if (noNamedAttribute.contains(attribute.name)) {
        final classAttribute = ClassAttribute(
          type: attribute.type,
          name: attribute.name,
          isNamed: false,
        );
        classAttributes.add(classAttribute);
      }
    }
  }
  return classAttributes;
}

List<ClassAttribute> getNamedClassAttributes(
  String constructor,
  List<Attribute> attributes,
) {
  List<ClassAttribute> classAttributes = [];
  final namedAttributes = constructor.getAttributesFromConstructor('{', '}');
  for (final namedAttribute in namedAttributes) {
    for (final attribute in attributes) {
      if (namedAttribute.contains(attribute.name)) {
        final classAttribute = ClassAttribute(
          type: attribute.type,
          name: attribute.name,
          isNamed: true,
          isRequired: namedAttribute.containsRequired,
          defaultValue: namedAttribute.getDefaultValue,
        );
        classAttributes.add(classAttribute);
      }
    }
  }
  return classAttributes;
}

String extractClassNameFromLine(String line) => line.split(' ')[1];

Future<List<File>> filterFilesWithReusableWidgets(List<File> files) async {
  List<File> filesWithReusableWidgets = [];
  for (final file in files) {
    final lines = await convertFileToLines(file);
    for (final line in lines) {
      if (line.startsWithReusableWidget) {
        filesWithReusableWidgets.add(file);
        break;
      }
    }
  }
  return filesWithReusableWidgets;
}

Future<File?> getFileWithReusableTheme(List<File> files) async {
  for (final file in files) {
    final stringValue = await convertFileToString(file);
    if (stringValue.containsReusableTheme) {
      return file;
    }
  }
  return null;
}

Future<String> convertFileToString(File file) async {
  return await file.readAsString();
}

Future<List<String>> convertFileToLines(File file) async {
  return await file.readAsLines();
}

List<File> getEveryDartFile(List<FileSystemEntity> entities) {
  final List<File> files = [];
  for (FileSystemEntity entity in entities) {
    if (entity is File && entity.path.isDartFile) {
      files.add(entity);
    }
  }
  return files;
}
