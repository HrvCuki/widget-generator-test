import 'dart:math';

import 'package:reusable_widgets_docs_generator/src/annotations/reusable_widget.dart';
import 'package:reusable_widgets_docs_generator/src/extensions/string_extensions.dart';

dynamic getRandomValue(String valueType, ReusableWidget reusableWidget) {
  if (valueType.isList) {
    final listValueType = valueType.getListValue;
    return List.generate(
      3,
      (_) => getInnerValue(listValueType, reusableWidget),
    );
  }
  return getInnerValue(valueType, reusableWidget);
}

dynamic getInnerValue(String valueType, ReusableWidget reusableWidget) {
  const questionMark = '?';
  final isOptional = valueType.endsWith(questionMark);
  final value = isOptional ? valueType.replaceAll(questionMark, '') : valueType;
  final flutterValue = value.mapToFlutterValue;
  if (flutterValue != null) return flutterValue.randomValue(reusableWidget);
  return null;
}

enum FlutterValues {
  int('int'),
  double('double'),
  string('String'),
  bool('bool'),
  color('Color'),
  //TODO: add Function(T value)
  function('Function()'),
  widget('Widget'),
  edgeInsets('EdgeInsets');

  final String key;

  const FlutterValues(this.key);
}

extension FlutterRandomValue on FlutterValues {
  static final _defaultRandomValue = {
    FlutterValues.int: Random().nextInt(1000),
    FlutterValues.double: Random().nextDouble(),
    FlutterValues.string: '"Placeholder"',
    FlutterValues.color: 'Color(${Random().nextInt(0xffffffff)})',
    FlutterValues.function: '() {}',
    FlutterValues.bool: true,
    FlutterValues.widget:
        'const Placeholder(fallbackHeight: 50, fallbackWidth: 50)',
    FlutterValues.edgeInsets: 'const EdgeInsets.all(8.0)',
  };

  dynamic randomValue(ReusableWidget reusableWidget) {
    switch (this) {
      case FlutterValues.color:
        if (reusableWidget.colorReplacement != null) {
          return reusableWidget.colorReplacement;
        }
        return _defaultRandomValue[this];
      case FlutterValues.widget:
        if (reusableWidget.widgetReplacement != null) {
          return reusableWidget.widgetReplacement;
        }
        return _defaultRandomValue[this];
      default:
        return _defaultRandomValue[this];
    }
  }
}
