import 'package:reusable_widgets_docs_generator/src/annotations/reusable_theme.dart';
import 'package:reusable_widgets_docs_generator/src/annotations/reusable_widget.dart';
import 'package:reusable_widgets_docs_generator/src/utils/random_value_generator.dart';

const dartExtension = '.dart';
const importText = 'import';
const classText = 'class';
const finalText = 'final';
const widgetBuildText = 'Widget build';
const requiredText = 'required';
const themeDataText = 'ThemeData';
const listText = 'List';
const foundationImport = "import 'package:flutter/foundation.dart';";

extension StringExtension on String {
  bool get isImport => startsWith(importText);

  bool get startsWithReusableWidget => startsWith(ReusableWidget.annotation);

  bool get containsReusableTheme => contains(ReusableTheme.annotation);

  bool get startsWithClass => startsWith(classText);

  bool get startsWithFinal => startsWith(finalText);

  bool get containsWidgetBuild => contains(widgetBuildText);

  bool get containsRequired => contains(requiredText);

  bool get isDartFile => endsWith(dartExtension);

  bool get isList => startsWith(listText);

  String get getListValue {
    final start = indexOf('<');
    final end = indexOf('>');
    return substring(start + 1, end);
  }

  String? get getDefaultValue {
    const equalSign = '=';
    if (contains(equalSign)) {
      return split(equalSign).map((e) => e.trim()).toList().last;
    }
    return null;
  }

  String get removeNamedArguments {
    final regex = RegExp("{.*?}");
    return replaceAllMapped(regex, (match) => '');
  }

  String getStringBetween(
    String startChar,
    String endChar, {
    bool fromFirstOccurrence = true,
  }) {
    final start = indexOf(startChar);
    final end = fromFirstOccurrence ? indexOf(endChar) : lastIndexOf(endChar);
    return substring(start + 1, end);
  }

  List<String> getAttributesFromConstructor(String startChar, String endChar) {
    const comma = ',';
    String text = this;
    text = removeLastOccurrenceOf(comma);
    return text.getStringBetween(startChar, endChar).split(comma);
  }

  String removeLastOccurrenceOf(String textToRemove) {
    final lastIndex = lastIndexOf(textToRemove);
    if (lastIndex >= 0) {
      return replaceRange(lastIndex, lastIndex + 1, '');
    }
    return this;
  }

  String get removeLastChar => substring(0, length - 1);

  FlutterValues? get mapToFlutterValue {
    final allValues = FlutterValues.values.map((e) => e.key);
    if (allValues.contains(this)) {
      return FlutterValues.values.firstWhere((e) => e.key == this);
    }
    print(
        '$this is still not supported with the script. Feel free to contribute and make a MR. Thanks :)');
    return null;
  }
}

extension StringListExtension on List<String> {
  String get buildNewLines => join('\n');
}
