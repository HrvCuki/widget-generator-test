touch lib/main_reusable_widgets.dart #Creates main_reusable_widgets, widget for displaying every reusable widget
dart reusable_widgets_docs_generator/lib/src/reusable_widgets_docs_generator_main.dart #Call the generator script
dart fix --apply lib/main_reusable_widgets.dart  #Fix dart issues
flutter format lib/main_reusable_widgets.dart #Format code