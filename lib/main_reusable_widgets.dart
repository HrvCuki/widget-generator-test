import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:reusable_widgets_docs_generator/reusable_widgets_docs_generator.dart';
import 'package:widget_generator_test/widgets/reusable_card.dart';
import 'package:widget_generator_test/widgets/reusable_list.dart';
import 'package:widget_generator_test/widgets/reusable_tab_bar_view.dart';
import 'package:widget_generator_test/widgets/reusable_list_tile.dart';


 void main() => runApp(const ReusableWidgetExample());

class ReusableWidgetExample extends StatelessWidget {
  const ReusableWidgetExample({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reusable widgets documentation',
      theme: theme,
      home: Scaffold(
        appBar: AppBar(title: const Text('Reusable widgets documentation')),
        body: ListView.separated(
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.symmetric(
              horizontal: kIsWeb ? MediaQuery.of(context).size.width / 5 : 16,
              vertical: 16,
            ),
            child: output[index],
          ),
          separatorBuilder: (_, __) => const Divider(height: 20),
          itemCount: 5,
        ),
      ),
    );
  }
}

final  output = [Column(children: [Text('ReusableCard', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),ReusableCard(
								text: "Placeholder",
								color: Color(1020832246),
),],),
Column(children: [Text('ReusableList', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),SizedBox(height: 200.0, child: ReusableList(
								children: [ReusableListTile(    "title",    color: Colors.green,    boolValue: true,    leading: Icon(Icons.ac_unit),    padding: EdgeInsets.all(10),  ), ReusableListTile(    "title",    color: Colors.green,    boolValue: true,    leading: Icon(Icons.ac_unit),    padding: EdgeInsets.all(10),  ), ReusableListTile(    "title",    color: Colors.green,    boolValue: true,    leading: Icon(Icons.ac_unit),    padding: EdgeInsets.all(10),  )],
),),],),
Column(children: [Text('Reusable123', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),Reusable123(
								text: "Placeholder",
								number: 421,
),],),
Column(children: [Text('ReusableTabBarView', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),SizedBox(height: 200.0, child: ReusableTabBarView(
								tabBarTitles: ["Placeholder", "Placeholder", "Placeholder"],
								tabBarPages: [ReusableCard(    text: "Card text",    color: Colors.blue,  ), ReusableCard(    text: "Card text",    color: Colors.blue,  ), ReusableCard(    text: "Card text",    color: Colors.blue,  )],
								spacingBetweenTabAndBody: 8,
								tabBarPadding: const EdgeInsets.all(8),
),),],),
Column(children: [Text('ReusableListTile', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),ReusableListTile(
								"Placeholder",
								info: "default",
								color: Color(1020832246),
								boolValue: true,
								leading: const Placeholder(fallbackHeight: 50, fallbackWidth: 50),
								padding: const EdgeInsets.all(8.0),
),],),
];

final theme = ThemeData(backgroundColor: Colors.blue,textTheme: textTheme,tabBarTheme: appTabBarTheme,);
const textTheme = TextTheme(headline1: TextStyle(color: Colors.red),);const appRadius = Radius.circular(8.0);const appBorderRadius = BorderRadius.all(appRadius);const appTabBarTheme = TabBarTheme(indicator: BoxDecoration(color: Colors.black,borderRadius: appBorderRadius,),labelColor: Colors.white,indicatorSize: TabBarIndicatorSize.label,unselectedLabelColor: Colors.black,);
