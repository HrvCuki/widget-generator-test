import 'package:flutter/material.dart';
import 'package:reusable_widgets_docs_generator/reusable_widgets_docs_generator.dart';

@ReusableWidget()
class ReusableCard extends StatelessWidget {
  final String text;
  final Color color;

  const ReusableCard({
    Key? key,
    required this.text,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(text),
          ],
        ),
      ),
    );
  }
}
