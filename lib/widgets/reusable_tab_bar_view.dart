import 'package:flutter/material.dart';
import 'package:reusable_widgets_docs_generator/reusable_widgets_docs_generator.dart';
import 'package:widget_generator_test/widgets/reusable_card.dart';

@ReusableWidget(
  fixedHeight: 200,
  widgetReplacement: ReusableCard(
    text: "Card text",
    color: Colors.blue,
  ),
  colorReplacement: Colors.blue,
)
class ReusableTabBarView extends StatelessWidget {
  final List<String> tabBarTitles;
  final List<Widget> tabBarPages;
  final Color? tabBackgroundColor;
  final double spacingBetweenTabAndBody;
  final EdgeInsetsGeometry tabBarPadding;

  const ReusableTabBarView({
    super.key,
    required this.tabBarTitles,
    required this.tabBarPages,
    this.tabBackgroundColor,
    this.spacingBetweenTabAndBody = 8,
    this.tabBarPadding = const EdgeInsets.all(8),
  }) : assert(tabBarPages.length == tabBarTitles.length);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabBarTitles.length,
      child: Column(
        children: [
          Container(
            color: tabBackgroundColor,
            padding: tabBarPadding,
            child: TabBar(
              padding: tabBarPadding,
              isScrollable: true,
              tabs: tabBarTitles
                  .map(
                    (e) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Tab(text: e),
                    ),
                  )
                  .toList(),
            ),
          ),
          SizedBox(height: spacingBetweenTabAndBody),
          Expanded(
            child: TabBarView(children: tabBarPages),
          ),
        ],
      ),
    );
  }
}
