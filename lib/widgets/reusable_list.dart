import 'package:flutter/material.dart';
import 'package:reusable_widgets_docs_generator/reusable_widgets_docs_generator.dart';
import 'package:widget_generator_test/widgets/reusable_list_tile.dart';

@ReusableWidget(
  fixedHeight: 200,
  widgetReplacement: ReusableListTile(
    "title",
    color: Colors.green,
    boolValue: true,
    leading: Icon(Icons.ac_unit),
    padding: EdgeInsets.all(10),
  ),
)
class ReusableList extends StatelessWidget {
  final List<Widget> children;

  const ReusableList({Key? key, required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: children.length,
      itemBuilder: (context, index) => children[index],
      separatorBuilder: (context, index) => const Divider(),
    );
  }
}

@ReusableWidget()
class Reusable123 extends StatelessWidget {
  final String text;
  final int number;
  const Reusable123({Key? key, required this.text, required this.number})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(text),
            Text(number.toString()),
            const Icon(Icons.access_time_sharp),
          ],
        ),
      ],
    );
  }
}
