import 'package:flutter/material.dart';
import 'package:reusable_widgets_docs_generator/reusable_widgets_docs_generator.dart';
import 'package:widget_generator_test/theme.dart';

@ReusableWidget()
class ReusableListTile extends StatelessWidget {
  final String title;
  final String info;
  final Color? color;
  final Function()? onPressed;
  final bool boolValue;
  final Widget? leading;
  final EdgeInsets padding;

  const ReusableListTile(
    this.title, {
    this.info = "default",
    required this.color,
    required this.boolValue,
    required this.leading,
    required this.padding,
    this.onPressed,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: appBorderRadius,
      child: Material(
        color: color,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: padding,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: appBorderRadius,
            ),
            child: Row(
              children: [
                if (leading != null)
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: leading,
                  ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(title),
                      Text(info),
                    ],
                  ),
                ),
                const Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.arrow_right,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
