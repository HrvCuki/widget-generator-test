import 'package:flutter/material.dart';
import 'package:reusable_widgets_docs_generator/reusable_widgets_docs_generator.dart';

@ReusableTheme()
final kornTheme = ThemeData(
  backgroundColor: Colors.blue,
  textTheme: textTheme,
  tabBarTheme: appTabBarTheme,
);

const textTheme = TextTheme(
  headline1: TextStyle(color: Colors.red),
);

const appRadius = Radius.circular(8.0);
const appBorderRadius = BorderRadius.all(appRadius);
const appTabBarTheme = TabBarTheme(
  indicator: BoxDecoration(
    color: Colors.black,
    borderRadius: appBorderRadius,
  ),
  labelColor: Colors.white,
  indicatorSize: TabBarIndicatorSize.label,
  unselectedLabelColor: Colors.black,
  // labelStyle: headline5,
  // unselectedLabelStyle: headline5,
);
